const DEFAULT_VALUE = 'https://old.reddit.com';

async function getRedirectURL(): Promise<string> {
  const store = await browser.storage.sync.get('reddit');
  return store.reddit || DEFAULT_VALUE;
}

async function setRedirectURL(url: string): Promise<void> {
  await browser.storage.sync.set({ reddit: url });
  console.log('seturl to ' + url);
}

function isValidURL(url: string): boolean {
  try {
    return Boolean(new URL(url));
  } catch {
    return false;
  }
}

async function main(): Promise<void> {
  const input = document.querySelector('input') as HTMLInputElement;
  const submit = document.querySelector('button') as HTMLButtonElement;

  input.defaultValue = await getRedirectURL();
  submit.addEventListener('click', async (event) => {
    event.preventDefault();
    if (isValidURL(input.value)) {
      await setRedirectURL(input.value);
    } else {
      // TODO: handle invalid urls visually
      // input.setCustomValidity('Invalid URL.');
    }
  });
}

main().catch((error) => console.error(error));
