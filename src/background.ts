const redirectFrom = [
  'www.reddit.com',
  'np.reddit.com',
  'new.reddit.com',
  'amp.reddit.com',
  'old.reddit.com',
];

let redirectTo = 'https://old.reddit.com';

// Load variable from storage.
browser.storage.sync.get('reddit').then((store) => {
  if (store.reddit) {
    redirectTo = store.reddit;
  }
});

// Store the default redirect in storage when installed
browser.runtime.onInstalled.addListener(async () => {
  await browser.storage.sync.set({ reddit: redirectTo });
});

// Listen for changes from options page
// https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/storage/StorageChange
browser.storage.onChanged.addListener((changes) => {
  if (changes.reddit) {
    redirectTo = changes.reddit.newValue;
  }
});

// "This event is triggered when a request is about to be made, and
// before headers are available. This is a good place to listen if
// you want to cancel or redirect the request."
// https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/webRequest
browser.webRequest.onBeforeRequest.addListener(
  (request) => {
    // Parse the requested url string into a URL object.
    // https://developer.mozilla.org/en-US/docs/Web/API/URL
    const url = new URL(request.url);
    if (redirectFrom.includes(url.host)) {
      // We check that we're not trying to redirect from our destination.
      // This is because a user may choose old.reddit.com or one of the
      // other reddit alternatives as their destination url.
      const redirectToHost = new URL(redirectTo).host;
      if (url.host !== redirectToHost) {
        console.log(url.host, redirectTo);
        // Build the full url and redirect
        const redirectUrl = redirectTo + url.pathname + url.search;
        // Return a "BlockingResponse"
        // https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/webRequest/BlockingResponse
        return { redirectUrl };
      }
    }
  },
  {
    // Match all HTTP, HTTPS and WebSocket URLs that are hosted at
    // "reddit.com" or one of its subdomains.
    urls: ['*://*.reddit.com/*'],
    // "main_frame: Top-level documents loaded into a tab."
    // https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/webRequest/ResourceType
    types: ['main_frame'],
  },
  ['blocking']
);
