# Changelog

All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [0.2.0] - 2020-12-12
 
### Fixed

- Custom endpoint will now be loaded from storage when browser is opened.

## [0.1.0] - 2020-12-11

### Added

- Initial release
   