# Reddit Redirect - Firefox extension

This is a firefox extension that redirects requests from reddit.com to the endpoint of your choice. Defaults to old.reddit.com. If you require redirects from Twitter, YouTube, Instagram, or Google Maps I highly recommend using [Privacy Redirect](https://github.com/SimonBrazell/privacy-redirect).

## Build

```bash
npm install --only=production
npm run build
```

## License

Reddit Redirect is open-source software licensed under the MIT license.

## Attributions

- [Icon](https://www.flaticon.com/free-icon/reddit_3884583?term=reddit&page=1&position=18) by [Freepik](https://www.flaticon.com/authors/freepik)

- [Privacy Redirect](https://github.com/SimonBrazell/privacy-redirect) by Simon Brazell was used as both inspiration and reference.